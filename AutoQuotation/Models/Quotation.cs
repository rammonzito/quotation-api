using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace AutoQuotation.Models
{
    public class Quotation
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }

        [BsonElement("Description")]
        public string Description {get; set;}

        // [BsonDateTimeOptions]
        // public DateTime Created { get; set; }

    }
}