namespace AutoQuotation.Models
{
    public class QuotationDatabaseSettings : IQuotationDatabaseSettings
    {
        public string QuotationCollectioName { get; set; }
        public string ConnectionString { get; set; }
        public string DatabaseName { get; set; }

    }

    public interface IQuotationDatabaseSettings
    {
        string QuotationCollectioName { get; set; }
        string ConnectionString { get; set; }
        string DatabaseName { get; set; }
    }
}