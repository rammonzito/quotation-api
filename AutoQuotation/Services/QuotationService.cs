using System.Collections.Generic;
using AutoQuotation.Models;
using MongoDB.Driver;

namespace AutoQuotation.Services
{
    public class QuotationService
    {
        private readonly IMongoCollection<Quotation> _quotations;

        public QuotationService(IQuotationDatabaseSettings settings)
        {
            var client = new MongoClient(settings.ConnectionString);
            var database = client.GetDatabase(settings.DatabaseName);

            _quotations = database.GetCollection<Quotation>(settings.QuotationCollectioName);
        }
        public List<Quotation> Get() =>
            _quotations.Find(quotation => true).ToList();

        public List<Quotation> GetByDescription(string description) =>
            _quotations.Find(quotation => quotation.Id == description).ToList();

        public Quotation Get(string id) =>
            _quotations.Find<Quotation>(quotation => quotation.Id == id).FirstOrDefault();

        public Quotation Create(Quotation quotation)
        {
            _quotations.InsertOne(quotation);
            return quotation;
        }

        public void Update(string id, Quotation quotationIn) =>
            _quotations.ReplaceOne(quotation => quotation.Id == id, quotationIn);

        public void Delete(string id) =>
            _quotations.DeleteOne(quotation => quotation.Id == id);
    
    }
}