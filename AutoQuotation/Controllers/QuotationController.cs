﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoQuotation.CustomServices.Interface;
using AutoQuotation.Models;
using AutoQuotation.Services;
using Microsoft.AspNetCore.Mvc;

namespace AutoQuotation.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class QuotationController : ControllerBase
    {
        private readonly QuotationService _quotationService;
        public QuotationController(QuotationService quotationService)
        {
            _quotationService = quotationService;
        }

        #region Get Region

        [HttpGet]
        public ActionResult<List<Quotation>> Get() =>
            _quotationService.Get();

        [HttpGet("{id:length(24)}", Name = "GetQuotation")]
        public ActionResult<Quotation> Get(string id)
        {
            var Quotation = _quotationService.Get(id);

            if (Quotation == null)
                return NotFound();

            return Quotation;
        }

        [HttpGet("{description(50)}", Name = "GetQuotationByDescription")]
        public ActionResult<List<Quotation>> GetByDescription(string description)
        {
            var Quotations = _quotationService.GetByDescription(description);
            if (Quotations == null)
                return NotFound();
            
            return Quotations;
        }

        #endregion

        [HttpPost]
        public ActionResult<Quotation> Create(Quotation quotation)
        {
            _quotationService.Create(quotation);
            return CreatedAtRoute("GetQuotation", new { id = quotation.Id.ToString() }, quotation);
        }

        [HttpPut("{id:length(24)}")]
        public IActionResult Update(string id, Quotation quotationIn)
        {
            var quotation = _quotationService.Get(id);

            if (quotation == null)
                return NotFound();

            _quotationService.Update(id, quotationIn);

            return NoContent();
        }

        [HttpDelete("{id:length(24)}")]
        public IActionResult Delete (string id)
        {
            var quotation = _quotationService.Get(id);
            if (quotation == null)
                return NotFound();
            
            _quotationService.Delete(id);

            return NoContent();
        }
    }
}